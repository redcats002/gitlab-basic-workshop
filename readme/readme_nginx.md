# Webserver Nginx

Attrach nginx to frontend app

1. Create folder /nginx at the same level to /package.json

2. Create file nginx.conf in /nginx
nginx.conf
```conf
server {

  listen 80;

  location / {
    root   /usr/share/nginx/html;
    index  index.html index.htm;
    try_files $uri $uri/ /index.html;
  }

  error_page 500 502 503 504 /50x.html;

  location = /50x.html {
      root /usr/share/nginx/html;
  }

}
```