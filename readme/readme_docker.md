# Docker

## Dockerfile

```Dockerfile
# command to build
# docker build -t react_cicd .
# docker run -d --rm -p 4250:80 --name reactcicd react_cicd
# docker-compose up -d
FROM node:lts-alpine AS builder

# Specify where our app will live in the container
WORKDIR /app

# Copy the package.json to the container
COPY package*.json ./

# Prepare the container for building React
RUN npm install

COPY . .

# We want the production version
RUN npm run build

# Prepare nginx
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:stable-alpine
COPY --from=builder /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

## Related Documents

- [SETUP Server for Deployment](./readme_container_registry.md)
