# Pipeline (.gitlab-ci)\*\*\*\*

## Define CI/CD variable on gitlab settings

Variables store information, like passwords and secret keys, that you can use in job scripts. Each project can define a maximum of 8000 variables. L

1. SSH_PRIVATE_KEY = private key from ssh-keygen
2. PROD_SERVER_IP = server ip that we want to deploy to
3. APP_NAME = app name to run the project
4. Docker as container registry
   1. CI_REGISTRY_USER =
   2. CI_REGISTRY_PASSWORD
   3. CI_REGISTRY_IMAGE
5. GitLab as container registry
   1. CI_GITLAB_REGISTRY_USER
   2. CI_GITLAB_REGISTRY_PASSWORD
   3. CI_GITLAB_REGISTRY_IMAGE
6. CI_REGISTRY_ADDRESS = For IP_REGISTRY_ADDRESS

## Construct pipeline

1. Create .gitlab-ci.yml at root level of project

```yml
# Define Stages for ordering in script
stages:
  - build
  - test
  - docker-build
  - deploy

# This folder is cached between builds
# https://docs.gitlab.com/ee/ci/yaml/index.html#cache
cache:
  paths:
    - node_modules/

build:
  stage: build
  # Official framework image. Look for the different tagged releases at:
  # https://hub.docker.com/r/library/node/tags/
  image: node
  script:
    - echo "Start building App"
    - npm install
    - npm run build
    - echo "Build successfully!"
  artifacts:
    expire_in: 1 hour
    paths:
      - build
      - node_modules/

test:
  stage: test
  image: node
  script:
    - echo "Testing App"
    - CI=true npm run lint
    - echo "Test successfully!"

docker-build:
  stage: docker-build
  image: docker:latest
  only:
    - main
  services:
    - name: docker:24.0.3-dind
  # IT"S WORKS TO DOCKER HUB
  # before_script:
  #   - docker logout
  #   - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
  # script:
  #   - docker build --pull -t "$CI_REGISTRY_IMAGE" . #CI_REGISTRY_IMAGE can look at docker hub of our repository
  #   - docker push "$CI_REGISTRY_IMAGE"
  # IT"S WORKS TO GITHUB CONTAINER REGISTRY
  before_script:
    - docker logout
    - docker login -u "$CI_GITLAB_REGISTRY_USER" -p "$CI_GITLAB_REGISTRY_PASSWORD" registry.gitlab.com
  script:
    - docker build -t $CI_GITLAB_REGISTRY_IMAGE .
    - docker push $CI_GITLAB_REGISTRY_IMAGE
deploy:
  stage: deploy
  only:
    - main
  image: kroniak/ssh-client
  before_script:
    - echo "deploying app"
  script:
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - ls ~/.ssh
    - ssh -o StrictHostKeyChecking=no root@$PROD_SERVER_IP "docker pull $CI_GITLAB_REGISTRY_IMAGE"
    - ssh -o StrictHostKeyChecking=no root@$PROD_SERVER_IP "docker stop $APP_NAME || true && docker rm $APP_NAME || true"
    - ssh -o StrictHostKeyChecking=no root@$PROD_SERVER_IP "docker run -p 3001:80 -d --name $APP_NAME $CI_GITLAB_REGISTRY_IMAGE"
```

## Related Documents

- [SETUP Server for Deployment](./readme_container_registry.md)
