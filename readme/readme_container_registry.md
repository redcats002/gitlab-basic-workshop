# GitLab Container Registry

How to use the container that registry in GitLab

- If we didn't push yet

1. Authen to registry.gitlab to access container registry
`docker login registry.gitlab.com`

2. Build image from Dockerfile at .
`docker build -t registry.gitlab.com/redcats002/gitlab-basic-workshop .`

3. Push to gitlab registry
`docker push registry.gitlab.com/redcats002/gitlab-basic-workshop`

- If we pushed

1. Pull
`docker pull $CI_GITLAB_REGISTRY_IMAGE` 