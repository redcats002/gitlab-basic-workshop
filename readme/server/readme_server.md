# Linux Server

## Ubuntu (Digital Ocean)

### ssh to server

```bash
ssh -o StrictHostKeyChecking=no -i $SSH_PRIVATE_KEY root@$PROD_SERVER_IP

# ssh -o StrictHostKeyChecking=no -i $SSH_PRIVATE_KEY root@159.223.81.231 (my server)
```

### Setup ssh keypairs

[SETUP Server for Deployment](./readme_ssh.md)

### Download Docker, git on server

`apk add docker git`

[Install docKer](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)

## Local server

### Build Docker container registry

`docker build -t registry.gitlab.com/redcats002/gitlab-basic-workshop.`

`docker push registry.gitlab.com/redcats002/gitlab-basic-workshop`

[Docker engine](https://docs.docker.com/engine/reference/run/#detached-vs-foreground)

### Create Dockerfile

```yaml
# command to build
# docker build -t react_cicd .
# docker run -d --rm -p 4250:80 --name reactcicd react_cicd
# docker-compose up -d
FROM node:lts-alpine AS builder

# [alpine image](https://www.baeldung.com/linux/install-docker-alpine-container)

# Specify where our app will live in the container
WORKDIR /app

# Copy the package.json to the container
COPY package*.json ./

# Prepare the container for building React
RUN npm install

COPY . .

# We want the production version
RUN npm run build

# Prepare nginx
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:stable-alpine
COPY --from=builder /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

### Build image from Dockerfile name alpine-docker

`docker build -t alpine-docker .`

### Run container from image & map volume

WINDOWS

`docker run -it -d --name alpine-docker -v "%cd%/var/run/docker.sock:/var/run/docker.sock:rw" alpine-docker`
`docker run -it -d --name alpine-docker -v "//var/run/docker.sock:/var/run/docker.sock:rw" alpine-docker`
`docker run -it -d --name alpine-docker -v "%cd%//var/run/docker.sock:/var/run/docker.sock:rw" alpine-docker`

macOS

`docker run -it -d --name alpine-docker -v "$(pwd)/var/run/docker.sock:/var/run/docker.sock:rw" alpine-docker`
`docker run -it -d --name alpine-docker -v "//var/run/docker.sock:/var/run/docker.sock:rw" alpine-docker`

### Shell to alpine-docker

`docker exec -it alpine-docker sh`

Install docker in alpine

`apk add docker`

### In alpine-docker sh

Docker login

`docker login -u "$CI_GITLAB_REGISTRY_USER" -p "$CI_GITLAB_REGISTRY_PASSWORD" registry.gitlab.com`
`docker login -u "redcats_002@hotmail.com" -p "060745Kan" registry.gitlab.com`

Pull & build

`docker pull registry.gitlab.com/redcats002/gitlab-basic-workshop`

Use this instead

`docker run -p 3005:80 -d --name vite-react-app registry.gitlab.com/redcats002/gitlab-basic-workshop`

## Related Documents

- [SETUP Server for Deployment](./readme_ssh.md)
