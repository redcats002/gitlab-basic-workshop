# SSH (Secure shell)

1. ssh-keygen -t rsa -C [user@mail.com] (Local)

2. copy .pub (Local)
`cat ~/Users/etc/hosts/id_rsa.pub`

3. vim to authorization (Server)
`vi ~/.ssh/authorzation`

4. Add public key (id_rsa.pub) to  authorization (Server)

AFTER THAT PIPELINE ON DEPLOY STAGE WILL WORK CORRECTLY
```yaml
deploy:
  stage: deploy
  image: kroniak/ssh-client
  before_script:
    - echo "deploying app"
  script:
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - ls ~/.ssh
    - ssh -o StrictHostKeyChecking=no root@$PROD_SERVER_IP "docker pull $CI_GITLAB_REGISTRY_IMAGE"
    - ssh -o StrictHostKeyChecking=no root@$PROD_SERVER_IP "docker stop $DO_APP_NAME || true && docker rm $DO_APP_NAME || true"
    - ssh -o StrictHostKeyChecking=no root@$PROD_SERVER_IP "docker run -p 3001:80 -d --name $DO_APP_NAME $CI_GITLAB_REGISTRY_IMAGE"
```