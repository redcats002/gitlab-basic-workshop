# GitLab CI/CD Workshop

Visit the result: <http://159.223.81.231/>

Google slide : <https://docs.google.com/presentation/d/1zw1UzhqExACjtRHwN5-7IjCGP3Sy8f5QsuQHC4tP7KI/edit?usp=sharing>

## Workshop workflow & concept 🚀

![cicd-process](./public/workflow.png "ci/cd-process")

### Workshop Instructions 📃

- [SETUP project](./readme/readme_setup.md)
- [SETUP Docker](./readme/readme_docker.md)
- [SETUP Webserver Nginx](./readme/readme_nginx.md)
- [CREATE Pipeline (.gitlab-ci.yml)](./readme/readme_pipeline_gitlab-ci.md)
- [SETUP Server for Deployment](./readme/server/readme_server.md)

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```bash
cd existing_repo
git remote add origin https://gitlab.com/redcats002/gitlab-basic-workshop.git
git branch -M main
git push -uf origin main
```

## TESTS
