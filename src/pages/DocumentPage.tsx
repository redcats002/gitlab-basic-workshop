import { useNavigate } from "react-router-dom";

export default function DocumentPage() {
  const navigate = useNavigate();

  return (
    <>
      <h1>Documents 📃</h1>
      <h3>In progress ...</h3>
      <p>
        <button
          style={{
            color: "black",
            backgroundColor: "whitesmoke",
          }}
          onClick={() => {
            navigate("/");
          }}
        >
          Back to home
        </button>
      </p>
    </>
  );
}
